package android.example.com.assignmentapps;

import com.google.android.gms.maps.model.LatLng;

public class GetterSetterPosition {

    public LatLng getPositionLL() {
        return PositionLL;
    }

    public void setPositionLL(LatLng positionLL) {
        PositionLL = positionLL;
    }

    private LatLng PositionLL;


}
