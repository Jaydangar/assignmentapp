package android.example.com.assignmentapps;

import com.google.android.gms.maps.model.LatLng;

public class GetterSetterDestination {

    private LatLng DestinationLL;

    public LatLng getDestinationLL() {
        return DestinationLL;
    }

    public void setDestinationLL(LatLng destinationLL) {
        DestinationLL = destinationLL;
    }
}
